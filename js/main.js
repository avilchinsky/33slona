function issuesController($scope) {

    $scope.issues = [];
    $scope.editableIssue = -1;
    $scope.issuesCounter = -1;

    /**
     * Создание задачи
     */
    $scope.addIssue = function () {
        var text = $.trim($scope.issueText);
        if (text.length) {
            if ($scope.editableIssue == -1) {
                var id = ++$scope.issuesCounter;
                $scope.fillIssues();
                $scope.issues.push({text: text, date: $scope.parseIssueDate(text), resolved: false, id: id});
            } else {
                $scope.fillIssues();
                var id = $scope.editableIssue;
                var index = $scope.getIssueIndex(id);
                if (index >= 0) {
                    var date = $scope.parseIssueDate(text);
                    $scope.issues[index].text = text;
                    if (date != $scope.issues[index].date) {
                        $scope.issues[index].date = date;
                    }
                    $scope.issues[index].resolved = false;
                }
            }
            $scope.issueText = '';
            $scope.editableIssue = -1;
            $scope.saveIssues();
        }
    }


    /**
     * Редактирование задачи
     * @param id
     */
    $scope.editIssue = function (id) {
        var index = $scope.getIssueIndex(id);
        if (index >= 0) {
            var issue = $scope.issues[index];
        }
        $scope.issueText = issue.text;
        $scope.editableIssue = id;
    }

    /**
     * Выполнение задачи
     * @param id
     */
    $scope.resolveIssue = function (id) {
        $scope.fillIssues();
        var index = $scope.getIssueIndex(id);
        if (index >= 0) {
            $scope.issues[index].resolved = !$scope.issues[index].resolved;
            $scope.saveIssues();
        }
    }

    /**
     * Удаление задачи
     * @param id
     */
    $scope.removeIssue = function (id) {
        if (confirm('Удалить запись?')) {
            $scope.fillIssues();
            var index = $scope.getIssueIndex(id);
            if (index >= 0) {
                $scope.issues.splice(index, 1);
                $scope.saveIssues();
            }
        }
    }

    /**
     * Нерешенные задачи
     * @param id
     */
    $scope.unresolvedIssuesLength = function () {
        var unresolvedIssues = _.filter($scope.issues, function (issue) {
            return !issue.resolved
        });
        return unresolvedIssues.length;
    }

    /**
     * Получение индекса задачи в коллекции по ID задачи
     * @param id
     */
    $scope.getIssueIndex = function (id) {
        var index = -1;
        var ln = $scope.issues.length;
        if (ln > 0) {
            for (var i = 0; i < ln; i++) {
                if ($scope.issues[i].id == id) {
                    return i;
                }
            }
        }
        return index;
    }

    /**
     * Восстановление задач
     */
    $scope.fillIssues = function (force) {
        if (typeof force == 'undefined') {
            force = false;
        }
        var issuesChanged = $.localStorage('issuesChanged');
        var issuesCounter = -1;
        if (typeof issuesChanged == 'undefined' || issuesChanged || force) {
            var issues = $.localStorage('issues');
            if (!issues) {
                issues = [];
            } else {
                issues = $.evalJSON(issues);
                if (issues.length > 0) {
                    issuesCounter = _.max(issues,function (issue) {
                        return parseInt(issue.id);
                    }).id;
                }
            }
            $scope.issues = issues;
            $scope.issuesCounter = issuesCounter;
            setTimeout(function () {
                $.localStorage('issuesChanged', false);
            }, 1000);
        }
    }

    /**
     * Сохранение задач
     */
    $scope.saveIssues = function () {
        var issues = $scope.issues;
        if (issues.length) {
            for (var i = 0; i < issues.length; i++) {
                delete issues[i]['$$hashKey'];
            }
        }
        $.localStorage('issues', $.toJSON(issues));
        $.localStorage('issuesChanged', true);
    }

    /**
     * Сортировка задач
     * @param issue
     */
    $scope.issuesOrder = function (issue) {
        var today = moment();
        var minDate = moment('01.01.2000', 'DD.MM.YYYY');
        day = (issue.date == '' || issue.resolved) ? minDate : moment(issue.date, 'DD.MM.YYYY');
        return today - day;
    }

    $scope.parseIssueDate = function (text) {

        var dayMonthParse = function (string) {
            string = string.replace(/\s+/, ' ');
            moment.lang('ru');
            var day = moment(string, "DD MMMM");
            if (!day.isValid()) {
                day = moment(string, "D MMMM");
            }
            if (day.isValid()) {
                return moment(day).format("DD.MM.YYYY");
            }

            return '';
        }

        var beforeYesterdayParse = function () {
            var today = moment();
            var beforeYesterday = today.subtract('days', 2);
            return moment(beforeYesterday).format("DD.MM.YYYY");
        }

        var yesterdayParse = function () {
            var today = moment();
            var yesterday = today.subtract('days', 1);
            return moment(yesterday).format("DD.MM.YYYY");
        }

        var todayParse = function () {
            var today = moment();
            return moment(today).format("DD.MM.YYYY");
        }

        var tomorrowParse = function () {
            var today = moment();
            var tomorrow = today.add('days', 1);
            return moment(tomorrow).format("DD.MM.YYYY");
        }

        var afterTomorrowParse = function () {
            var today = moment();
            var afterTomorrow = today.add('days', 2);
            return moment(afterTomorrow).format("DD.MM.YYYY");
        }

        var dateParse = function (string) {
            moment.lang('ru');
            var day = moment(string, "DD.MM.YYYY");
            if (!day.isValid()) {
                day = moment(string, "D.MM.YYYY");
            }
            if (!day.isValid()) {
                day = moment(string, "DD.M.YYYY");
            }
            if (!day.isValid()) {
                day = moment(string, "D.M.YYYY");
            }
            if (day.isValid()) {
                return moment(day).format("DD.MM.YYYY");
            }

            return '';

        }

        var regexps = [
            {regexp: /(\d{1,2}\s[а-я]+)/img, handler: dayMonthParse},
            {regexp: /(позавчера)/img, handler: beforeYesterdayParse},
            {regexp: /(вчера)/img, handler: yesterdayParse},
            {regexp: /(сегодня)/img, handler: todayParse},
            {regexp: /(послезавтра)/img, handler: afterTomorrowParse},
            {regexp: /(завтра)/img, handler: tomorrowParse},
            {regexp: /(\d{1,2}\.\d{1,2}\.\d{4})/img, handler: dateParse}
        ];
        for (var i = 0; i < regexps.length; i++) {
            var res = regexps[i].regexp.exec(text);
            if (res) {
                return regexps[i].handler(res[0]);
            }
        }
        return '';
    }

    /**
     * Просрочена ли задача
     * @param issue
     * @returns {boolean}
     */
    $scope.isExpired = function (issue) {
        if (issue.date != '' && !issue.resolved) {
            moment.lang('ru');
            var today = moment();
            var day = moment(issue.date, "DD.MM.YYYY");
            if (day < today) {
                return true;
            }
        }

        return false;
    }

    $scope.fillIssues(true);
    $scope.timer = setInterval(function () {
        $scope.$apply(function () {
            $scope.fillIssues();
        });
    }, 1000);

}